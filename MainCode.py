import sys
import os
import pygame
import pygame_menu
import random

control_tank1 = {pygame.K_a: [-5, 0], pygame.K_d: [5, 0], pygame.K_w: [0, -5], pygame.K_s: [0, 5]}
control_tank2 = {pygame.K_LEFT: [-5, 0], pygame.K_RIGHT: [5, 0], pygame.K_UP: [0, -5], pygame.K_DOWN: [0, 5]}

MAP1 = [[0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0]]

MAP2 = [[1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0],
        [1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1]]

MAP3 = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0],
        [0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0],
        [0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

MAPS = [MAP1, MAP2, MAP3]


def terminate():
    pygame.quit()
    sys.exit()


def load_image(name):
    fullname = os.path.join('data', name)
    image = pygame.image.load(fullname)
    return image


class Border(pygame.sprite.Sprite):
    def __init__(self, x1, y1, x2, y2, name):
        super().__init__(all_sprites)
        self.add(walls)
        if name == 'right' or name == 'left':  # вертикальная стенка
            self.image = pygame.Surface([1, y2 - y1])
            self.image.fill("grey")
            self.rect = pygame.Rect(x1, y1, 1, y2 - y1)
        else:  # горизонтальная стенка
            self.image = pygame.Surface([x2 - x1, 1])
            self.image.fill("grey")
            self.rect = pygame.Rect(x1, y1, x2 - x1, 1)


class Ball(pygame.sprite.Sprite):

    def __init__(self, radius, x, y, path, color):
        super().__init__(bullets)
        if color == (255, 0, 0):
            self.add(bullet1)
        else:
            self.add(bullet2)
        self.radius = radius
        self.image = pygame.Surface((2 * radius, 2 * radius),
                                    pygame.SRCALPHA, 32)
        pygame.draw.circle(self.image, pygame.Color(color),
                           (radius, radius), radius)
        self.rect = pygame.Rect(x, y, 2 * radius, 2 * radius)
        self.vx = 0
        self.vy = 0
        if path == "up":
            self.vy = -10
        elif path == "right":
            self.vx = 10
        elif path == "down":
            self.vy = 10
        elif path == "left":
            self.vx = -10

    def update(self):
        self.rect = self.rect.move(self.vx, self.vy)
        if pygame.sprite.spritecollideany(self, walls):
            self.kill()


class MoveTank1(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__(all_sprites)

        self.image = load_image("BlueTankRight.png")
        self.image = pygame.transform.scale(self.image, (60, 60))
        self.rect = pygame.Rect(x, y, 65, 65)
        self.vx = 0
        self.vy = 0
        self.direction = 'right'
        self.add(tanks1_sprites)

    def update(self):
        if self.direction == 'right':
            self.image = load_image("RedTankRight.png")
        elif self.direction == 'left':
            self.image = load_image("RedTankLeft.png")
        elif self.direction == 'up':
            self.image = load_image("RedTankUp.png")
        elif self.direction == 'down':
            self.image = load_image("RedTankDown.png")
        self.image = pygame.transform.scale(self.image, (60, 60))

        if pygame.sprite.spritecollideany(self, walls):
            all_sprites.clear(screen, background)
            #  sound3.play()
            self.kill()
            show_go_screen("BLUE")

    def against_bullets1(self):
        if pygame.sprite.spritecollideany(self, bullet2):
            #  sound2.play()
            show_go_screen("BLUE")
        self.rect = self.rect.move(self.vx, self.vy)


class MoveTank2(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__(all_sprites)

        self.image = load_image("BlueTankRight.png")
        self.image = pygame.transform.scale(self.image, (60, 60))
        self.rect = pygame.Rect(x, y, 65, 65)
        self.vx = 0
        self.vy = 0
        self.direction = 'left'
        self.add(tanks2_sprites)

    def update(self):
        if self.direction == 'right':
            self.image = load_image("BlueTankRight.png")
        elif self.direction == 'left':
            self.image = load_image("BlueTankLeft.png")
        elif self.direction == 'up':
            self.image = load_image("BlueTankUp.png")
        elif self.direction == 'down':
            self.image = load_image("BlueTankDown.png")
        self.image = pygame.transform.scale(self.image, (60, 60))

        if pygame.sprite.spritecollideany(self, walls):
            all_sprites.clear(screen, background)
            #  sound3.play()
            self.kill()
            show_go_screen("RED")

    def against_bullets2(self):
        if pygame.sprite.spritecollideany(self, bullet1):
            #  sound2.play()
            show_go_screen("RED")
        self.rect = self.rect.move(self.vx, self.vy)


class ImageWin(pygame.sprite.Sprite):
    image = load_image("game_over.jpg")

    def __init__(self, group):
        super().__init__(group)
        self.rect = self.image.get_rect()
        self.rect.x = 0
        self.rect.y = 0


def show_go_screen(name_of_win):
    clean_sprites()
    all_sprites1 = pygame.sprite.Group()
    ImageWin(all_sprites1)
    button_out = pygame.Rect(1375, 30, 200, 75)
    button_again = pygame.Rect(435, 600, 725, 110)
    button_menu = pygame.Rect(580, 760, 400, 75)
    while True:
        for i in pygame.event.get():
            if i.type == pygame.QUIT:
                terminate()
            if i.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = i.pos
                if button_out.collidepoint(mouse_pos):
                    terminate()
                if button_menu.collidepoint(mouse_pos):
                    game_menu()
                if button_again.collidepoint(mouse_pos):
                    start_the_game()
        all_sprites1.draw(screen)
        pygame.draw.rect(screen, [0, 0, 255], button_out)
        inscription(85, 'EXIT', 42, 'grey', 1405)

        pygame.draw.rect(screen, [112, 128, 255], button_again)
        inscription(150, 'PLAY AGAIN', 610, 'red', 480)

        pygame.draw.rect(screen, [0, 0, 128], button_menu)
        inscription(100, 'MENU', 765, (128, 0, 128), 682)

        if name_of_win == "RED":
            inscription(200, name_of_win, 100, 'red', 800)
            inscription(200, "WIN", 100, 'green', 500)
        elif name_of_win == "DRAW":
            inscription(200, name_of_win, 100, 'green', 750)
            inscription(200, "WIN", 100, 'green', 420)
        else:
            inscription(200, name_of_win, 100, 'blue', 800)
            inscription(200, "WIN", 100, 'green', 500)
        pygame.display.flip()
        clock.tick(60)


def move1(direction):
    if pygame.key.get_pressed()[pygame.K_a]:
        vx, vy = control_tank1[pygame.K_a]
        direction = "left"
    elif pygame.key.get_pressed()[pygame.K_d]:
        vx, vy = control_tank1[pygame.K_d]
        direction = "right"
    elif pygame.key.get_pressed()[pygame.K_w]:
        vx, vy = control_tank1[pygame.K_w]
        direction = "up"
    elif pygame.key.get_pressed()[pygame.K_s]:
        vx, vy = control_tank1[pygame.K_s]
        direction = "down"
    else:
        vx, vy = 0, 0
    return vx, vy, direction


def move2(direction):
    if pygame.key.get_pressed()[pygame.K_LEFT]:
        vx, vy = control_tank2[pygame.K_LEFT]
        direction = "left"
    elif pygame.key.get_pressed()[pygame.K_RIGHT]:
        vx, vy = control_tank2[pygame.K_RIGHT]
        direction = "right"
    elif pygame.key.get_pressed()[pygame.K_UP]:
        vx, vy = control_tank2[pygame.K_UP]
        direction = "up"
    elif pygame.key.get_pressed()[pygame.K_DOWN]:
        vx, vy = control_tank2[pygame.K_DOWN]
        direction = "down"
    else:
        vx, vy = 0, 0
    return vx, vy, direction


def start_the_game():
    screen.fill((0, 0, 0))
    tank1 = MoveTank1(100, 420)
    tank2 = MoveTank2(1500, 420)
    Border(5, 5, width - 5, 5, 'up')
    Border(5, height - 5, width - 5, height - 5, 'down')
    Border(5, 5, 5, height - 5, 'left')
    Border(width - 5, 5, width - 5, height - 5, 'right')
    blocks = pygame.sprite.Group()
    create_map()
    blocks.draw(screen)

    while True:
        for i in pygame.event.get():
            if i.type == pygame.QUIT:
                terminate()
            if i.type == pygame.KEYDOWN:

                if i.key == pygame.K_a:
                    tank1.vx, tank1.vy = -5, 0
                    tank1.direction = "left"
                elif i.key == pygame.K_d:
                    tank1.vx, tank1.vy = 5, 0
                    tank1.direction = "right"
                elif i.key == pygame.K_w:
                    tank1.vx, tank1.vy = 0, -5
                    tank1.direction = "up"
                elif i.key == pygame.K_s:
                    tank1.vx, tank1.vy = 0, 5
                    tank1.direction = "down"

                if i.key == pygame.K_LEFT:
                    tank2.vx, tank2.vy = -5, 0
                    tank2.direction = "left"
                elif i.key == pygame.K_RIGHT:
                    tank2.vx, tank2.vy = 5, 0
                    tank2.direction = "right"
                elif i.key == pygame.K_UP:
                    tank2.vx, tank2.vy = 0, -5
                    tank2.direction = "up"
                elif i.key == pygame.K_DOWN:
                    tank2.vx, tank2.vy = 0, 5
                    tank2.direction = "down"

                if i.key == pygame.K_SPACE:
                    #  sound1.play()
                    Ball(15, tank1.rect[0] + 15, tank1.rect[1] + 15, tank1.direction, (255, 0, 0))
                if i.key == pygame.K_KP_ENTER:
                    #  sound1.play()
                    Ball(15, tank2.rect[0] + 15, tank2.rect[1] + 15, tank2.direction, (0, 0, 255))

            if i.type == pygame.KEYUP:
                for j in range(4):
                    if i.key in [list(control_tank1.keys())[j]]:
                        tank1.vx, tank1.vy, tank1.direction = move1(tank1.direction)
                    if i.key in [list(control_tank2.keys())[j]]:
                        tank2.vx, tank2.vy, tank2.direction = move2(tank2.direction)

        all_sprites.update()
        screen.fill((0, 0, 0))

        bullets.draw(screen)
        bullets.update()

        hits = pygame.sprite.groupcollide(blocks, bullets, False, False)
        for hit in hits:
            hit.kill()

        hits21 = pygame.sprite.groupcollide(bullet1, bullet2, False, False)
        hits12 = pygame.sprite.groupcollide(bullet2, bullet1, False, False)
        for hit in hits21:
            hit.kill()
        for hit in hits12:
            hit.kill()

        if pygame.sprite.groupcollide(tanks1_sprites, tanks2_sprites, False, False):
            #  sound3.play()
            show_go_screen("DRAW")

        all_sprites.draw(screen)
        tank1.against_bullets1()
        tank2.against_bullets2()
        pygame.display.flip()
        clock.tick(60)


def clean_sprites():
    for i in all_sprites:
        i.kill()
    for i in bullet1:
        i.kill()
    for i in bullets:
        i.kill()
    for i in bullet2:
        i.kill()


def create_map():
    map_choice = MAPS[random.randint(0, 2)]
    for i in range(len(map_choice)):
        for j in range(len(map_choice[i])):
            if map_choice[i][j]:
                Border(j * 100, i * 100, j * 100 + 100, i * 100 * 2, 'down')  # верхнее ребро
                Border(j * 100, i * 100 + 100, j * 100 + 100, i * 100 + 100, 'up')  # нижнее ребро
                Border(j * 100, i * 100, j * 100, i * 100 + 100, 'right')  # левое ребро
                Border(j * 100 + 100, i * 100, j * 100 + 100, i * 100 + 100, 'left')  # правое ребро


def inscription(font, line, text_coord, color, x):
    font = pygame.font.Font(None, font)
    string_rendered = font.render(line, True, pygame.Color(color))
    intro_rect = string_rendered.get_rect()
    intro_rect.top = text_coord
    intro_rect.x = x
    text_coord += intro_rect.height
    screen.blit(string_rendered, intro_rect)


def rules_game():
    intro_text = ["Добро пожаловать в нашу игру 'PiTanks'.",
                  "Она создана по задумке старой игры 'танчики'.",
                  "Цель каждого игрока - выиграть противника, просто попав в него своим снарядом",
                  "Но сложность заключается в том, что на поле игры есть стены и границы, при заезде на которые",
                  "вы умираете. На большой скорости сложно маневрировать, добираясь до противника.",
                  "Также вы умрёте, если столкнётесь с другим танком. Но в этом случае будет ничья.",
                  " ",
                  "Двигаться можно в 4 стороны - вверх, вправо, вниз и влево.",
                  "Первый игрок управляет с помощью кнопок 'a', 'w', 'd', 's' и стреляет с помощью кнопки 'space'",
                  "Второй же, управляет с помощью стрелок и стреляет с помощью левой кнопки 'enter'",
                  "Желаем вам удачи в тестировании нашей игры!"]
    lines_coord_x = [500,
                     450,
                     180,
                     70,
                     150,
                     170,
                     0,
                     320,
                     70,
                     130,
                     440]
    text_return = ["RETURN"]

    screen.fill((0, 0, 0))
    text_coord = 100
    for i in range(len(intro_text)):
        inscription(45, intro_text[i], text_coord, 'green', lines_coord_x[i])
        text_coord += 65

    button_esc = pygame.Rect(1375, 30, 200, 75)
    pygame.draw.rect(screen, [0, 0, 255], button_esc)

    inscription(60, text_return[0], 50, 'green', 1387)

    while True:
        for i in pygame.event.get():
            if i.type == pygame.QUIT:
                terminate()
            if i.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = i.pos
                if button_esc.collidepoint(mouse_pos):
                    return
        pygame.display.flip()
        clock.tick(60)


def settings_game():
    volume = 0.5
    screen.fill((0, 0, 0))
    button_return = pygame.Rect(1315, 30, 265, 75)
    button_turn_off_sound = pygame.Rect(325, 135, 400, 75)
    button_continuation_of_sound = pygame.Rect(850, 135, 400, 75)
    button_turn_down_sound = pygame.Rect(325, 250, 400, 75)
    button_turn_up_sound = pygame.Rect(850, 250, 400, 75)

    button_music1 = pygame.Rect(325, 465, 400, 75)
    button_music2 = pygame.Rect(850, 465, 400, 75)
    button_music3 = pygame.Rect(325, 580, 400, 75)
    button_music4 = pygame.Rect(850, 580, 400, 75)
    button_music5 = pygame.Rect(325, 695, 400, 75)
    button_music6 = pygame.Rect(850, 695, 400, 75)
    while True:
        for i in pygame.event.get():
            if i.type == pygame.QUIT:
                terminate()
            if i.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = i.pos
                if button_return.collidepoint(mouse_pos):
                    return
                elif button_turn_off_sound.collidepoint(mouse_pos):
                    pygame.mixer.music.pause()
                elif button_continuation_of_sound.collidepoint(mouse_pos):
                    pygame.mixer.music.unpause()
                elif button_turn_down_sound.collidepoint(mouse_pos):
                    if volume > 0:
                        volume -= 0.1
                        #  change_sound_and_music_volume(volume)
                elif button_turn_up_sound.collidepoint(mouse_pos):
                    if volume < 1:
                        volume += 0.1
                        #  change_sound_and_music_volume(volume)
                elif button_music1.collidepoint(mouse_pos):
                    play_music(1)
                elif button_music2.collidepoint(mouse_pos):
                    play_music(2)
                elif button_music3.collidepoint(mouse_pos):
                    play_music(3)
                elif button_music4.collidepoint(mouse_pos):
                    play_music(4)
                elif button_music5.collidepoint(mouse_pos):
                    play_music(5)
                elif button_music6.collidepoint(mouse_pos):
                    play_music(6)
        inscription(115, 'SOUND CONTROL', 40, 'green', 450)
        inscription(115, 'MUSIC SELECTION', 365, (0, 191, 255), 435)

        pygame.draw.rect(screen, [0, 0, 255], button_return)
        inscription(85, 'RETURN', 42, 'grey', 1325)

        sound_control = ['PAUSE MUSIC', 'UNPAUSE MUSIC', 'TURN DOWN', 'TURN UP']
        buttons_sounds = [button_turn_off_sound, button_continuation_of_sound, button_turn_down_sound,
                          button_turn_up_sound]
        for i in range(2):
            draw_rectangle([119, 136, 153], buttons_sounds[i * 2], 60, sound_control[i * 2], 375 + 10 * i,
                           155 + 115 * i,
                           'white')
            draw_rectangle([119, 136, 153], buttons_sounds[1 + i * 2], 60, sound_control[1 + i * 2], 880 + 80 * i,
                           155 + 115 * i,
                           'white')

        musics = ['MUSIC 1', 'MUSIC 2', 'MUSIC 3', 'MUSIC 4', 'MUSIC 5', 'MUSIC 6']
        buttons_musics = [button_music1, button_music2, button_music3, button_music4, button_music5, button_music6]
        for i in range(3):
            draw_rectangle([75, 0, 130], buttons_musics[i * 2], 60, musics[i * 2], 440, 485 + 115 * i, (255, 0, 255))
            draw_rectangle([75, 0, 130], buttons_musics[1 + i * 2], 60, musics[i * 2 + 1], 975, 485 + 115 * i,
                           (255, 0, 255))

        pygame.display.flip()
        clock.tick(60)


def draw_rectangle(color_button, name_button, font, text, coord_x, coord_y, color_text):
    pygame.draw.rect(screen, color_button, name_button)
    inscription(font, text, coord_y, color_text, coord_x)


def play_music(number):
    #  pygame.mixer.music.load('music/music{}.mp3'.format(number))
    #  pygame.mixer.music.play(-1, 0.0)
    pass


#  def change_sound_and_music_volume(volume):
#  sound1.set_volume(volume)
#  sound2.set_volume(volume)
#  sound3.set_volume(volume)
#  pygame.mixer.music.set_volume(volume)


def game_menu():
    menu = pygame_menu.Menu(600, 500, title="PiTankS", theme=pygame_menu.themes.THEME_DARK)
    menu.add_button('Play', start_the_game, font_size=60)
    menu.add_button('Rules', rules_game, font_size=60)
    menu.add_button('Settings', settings_game, font_size=60)
    menu.add_button('Quit', pygame_menu.events.EXIT, font_size=60)
    menu.mainloop(screen)


pygame.init()
size = width, height = 1600, 900
background = pygame.Surface(size)

# pygame.mixer.music.set_volume(0.5)

# play_music(1)
# sound1 = pygame.mixer.Sound('music/sound1.mp3')
#  sound2 = pygame.mixer.Sound('music/sound2.mp3')
#  sound3 = pygame.mixer.Sound('music/sound3.mp3')
#  sound1.set_volume(0.5)
#  sound2.set_volume(0.5)
#  sound3.set_volume(0.5)

screen = pygame.display.set_mode(size)
pygame.display.set_caption('PiTankS')
clock = pygame.time.Clock()

all_sprites = pygame.sprite.Group()
tanks1_sprites = pygame.sprite.Group()
tanks2_sprites = pygame.sprite.Group()
bullets = pygame.sprite.Group()

bullet1 = pygame.sprite.Group()
bullet2 = pygame.sprite.Group()
walls = pygame.sprite.Group()

game_menu()
